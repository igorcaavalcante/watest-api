import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsOptional, IsString, MaxLength, Min, IsNumber } from 'class-validator';
import { IOrder } from 'modules/database/interfaces/order';

export class SaveValidator implements IOrder {
  @IsOptional()
  @IsInt()
  @Min(0)
  @ApiProperty({ required: false, type: 'integer' })
  public id?: number;

  @IsNotEmpty()
  @IsInt()
  @Min(0)
  @ApiProperty({ required: true, type: 'integer' })
  public quantity: number;

  @ApiProperty({ required: true, type: 'integer' })
  public userId: number;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  @ApiProperty({ required: true, type: 'float' })
  public value: number;

  @IsOptional()
  @IsString()
  @MaxLength(100)
  @ApiProperty({ required: false, type: 'string', maxLength: 100 })
  public description?: string;
}
