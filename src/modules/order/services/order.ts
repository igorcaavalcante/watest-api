import { BadRequestException, ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { ICurrentUser } from 'modules/common/interfaces/currentUser';
import { User } from 'modules/database/models/user';

import { OrderRepository } from '../repositories/order';
import { IOrder } from 'modules/database/interfaces/order';
import { Order } from 'modules/database/models/order';

@Injectable()
export class OrderService {
  constructor(private orderRepository: OrderRepository) {}

  public async save(model: IOrder): Promise<Order> {
    if (model.id) return this.update(model);
    const order = await this.orderRepository.insert(model);
    return order;
  }

  public async remove(orderId: number, currentUser: ICurrentUser): Promise<void> {
    const order = await this.orderRepository.findById(orderId);

    if (!order) {
      throw new NotFoundException('not-found');
    }

    if (order.id !== currentUser.id) {
      throw new BadRequestException('not-allowed-remove-order-from-other-user');
    }

    return this.orderRepository.remove(orderId);
  }

  private async update(model: IOrder): Promise<Order> {
    const order = await this.orderRepository.findById(model.id);
    if (!order) throw new NotFoundException('not-found');
    return this.orderRepository.update({ ...order, ...model });
  }
}
