import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags, ApiResponse } from '@nestjs/swagger';
import { Order } from 'modules/database/models/order';
import { SaveValidator } from '../validators/order/save';
import { OrderService } from '../services/order';
import { CurrentUser } from 'modules/common/guards/token';
import { ICurrentUser } from 'modules/common/interfaces/currentUser';

@ApiTags('Order: Order')
@Controller()
export class OrderController {
  constructor(private orderService: OrderService) {}

  @Post()
  @ApiResponse({ status: 200, type: Order })
  public async save(@Body() model: SaveValidator, @CurrentUser() currentUser: ICurrentUser) {
    console.log(currentUser);
    model.userId = currentUser.id;
    return this.orderService.save(model);
  }
}
