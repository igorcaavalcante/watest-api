import { HttpModule, MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { CommonModule } from 'modules/common/module';
import { DatabaseModule } from 'modules/database/module';
import { BindUserMiddleware } from 'modules/common/middlewares/bindUser';
import { OrderController } from './controllers/order';
import { OrderService } from './services/order';
import { OrderRepository } from './repositories/order';

@Module({
  imports: [HttpModule, CommonModule, DatabaseModule],
  controllers: [OrderController],
  providers: [OrderService, OrderRepository]
})
export class OrderModule implements NestModule {
  public configure(consumer: MiddlewareConsumer) {
    consumer.apply(BindUserMiddleware).forRoutes('*');
  }
}
