import { ApiProperty } from '@nestjs/swagger';
import { Model } from 'objection';

import { User } from './user';
import { IOrder } from '../interfaces/order';

export class Order extends Model implements IOrder {
  @ApiProperty({ type: 'string' })
  public id?: number;

  @ApiProperty({ type: 'integer' })
  public quantity: number;

  @ApiProperty({ type: 'float' })
  public value: number;

  @ApiProperty({ type: 'string' })
  public description?: string;

  @ApiProperty({ type: 'string', format: 'date-time' })
  public createdDate?: Date;

  @ApiProperty({ type: 'string', format: 'date-time' })
  public updatedDate?: Date;

  @ApiProperty({ type: 'integer' })
  public userId?: number;

  public user: User;

  public static get tableName(): string {
    return 'Order';
  }

  public static get relationMappings(): any {
    return {
      user: {
        relation: Model.HasOneRelation,
        modelClass: User,
        filter: (query: any) => query.select('id', 'firstName', 'lastName', 'email'),
        join: {
          from: 'User.id',
          to: 'Order.userId'
        }
      }
    };
  }

  public $beforeInsert(): void {
    this.createdDate = this.updatedDate = new Date();
  }

  public $beforeUpdate(): void {
    this.updatedDate = new Date();
  }

  public $formatJson(data: IOrder): IOrder {
    return data;
  }
}
