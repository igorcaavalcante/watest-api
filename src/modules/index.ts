import { Module } from '@nestjs/common';
import { RouterModule } from 'nest-router';

import { AdminModule } from './admin/module';
import { AppModule } from './app/module';
import { OrderModule } from './order/module';

@Module({
  imports: [
    RouterModule.forRoutes([
      { path: '/admin', module: AdminModule },
      { path: '/app', module: AppModule },
      { path: '/order', module: OrderModule }
    ]),
    AdminModule,
    AppModule,
    OrderModule
  ]
})
export class ApplicationModule {}
